#1 Count lines
cat ~/CMEECourseWork/Week1/Data/*.fasta | wc -l
#2 Print everything from 2nd line for E. coli
cat ~/CMEECourseWork/Week1/Data/E.coli.fasta | tail -n +2
#3 Count sequence length of E. coli genome:
cat ~/CMEECourseWork/Week1/Data/E.coli.fasta | tail -n +2 | wc -m
#4 Count matches of sequence ATGC
cat ~/CMEECourseWork/Week1/Data/E.coli.fasta | tail -n +2 | tr -d \n | grep -o ATGC | wc -l
#5 AT/GC ratio
echo "AT:GC= " && AT=`cat ~/CMEECourseWork/Week1/Data/E.coli.fasta | tail -n +2 | tr -d \n | grep -o AT | wc -l` && GC=`cat ~/CMEECourseWork/Week1/Data/E.coli.fasta | tail -n +2 | tr -d \n | grep -o GC | wc -l` && echo $AT":"$GC





 

