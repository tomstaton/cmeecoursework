"""Aligns sequences for external csv file"""

__author__ = 'Tom Staton'
__version__ = '0.0.1'

#!/usr/bin/env python
	
import csv

f = open('../Data/align-in.csv','rb')
g = open('../Data/align-out.txt', 'wb')

csvread=csv.reader(f)
csvwrite=csv.writer(g)

for row in csvread:
	seq1=row[0]
	seq2=row[1]

csvwrite.writerow
print "Adding output to 'align-out.txt'"
g.write(str("seq1 = "))
g.write(str(seq1) + '\n')
g.write(str("seq2 = "))
g.write(str(seq2) + '\n')

# These are the two sequences to match
# seq2 = "ATCGCCGGATTACGGG"
# seq1 = "CAATTCGGAT"

# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
	s1 = seq1
	s2 = seq2
else:
	s1 = seq2
	s2 = seq1
	l1, l2 = l2, l1 # swap the two lengths

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
	# startpoint is the point at which we want to start
	matched = "" # contains string for alignement
	score = 0
	for i in range(l2):
		if (i + startpoint) < l1:
			# if its matching the character
			if s1[i + startpoint] == s2[i]:
				matched = matched + "*"
				score = score + 1
			else:
				matched = matched + "-"

	# build some formatted output
	g.write(str("." * startpoint + matched) + '\n')         
	g.write(str("." * startpoint + s2) + '\n')
	g.write(str(s1) + '\n')
	g.write(str(score) + '\n') 
	g.write(str("") + '\n')

	return score

calculate_score(s1, s2, l1, l2, 0)
calculate_score(s1, s2, l1, l2, 1)
calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
	z = calculate_score(s1, s2, l1, l2, i)
	if z > my_best_score:
		my_best_align = "." * i + s2
		my_best_score = z

g.write(str(my_best_align) + '\n')
g.write(str(s1) + '\n')
g.write(str("Best score:"))
g.write(str(my_best_score) + '\n')

