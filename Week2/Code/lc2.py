"""Rainfall of >100mm or <50mm per month"""

__author__ = 'Tom Staton'
__version__ = '0.0.1'

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.
 
	
highrain = [x for x in rainfall if x[1]>100] 
print highrain
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

lowrain = [x for x in rainfall if x[1]<50] 
print lowrain

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

# Rain greater than 100 mm
highrain=[] #create empty list
for row in rainfall:
	if row[1]>100: #only adds tuple where 2nd value is >100
		highrain.append(tuple(row)) #adds these to the list
print highrain

# Rain less than 50 mm
lowrain=[] #create empty list
for row in rainfall:
	if row[1]<50: #only adds tuple where 2nd value is <50
		lowrain.append(tuple(row)) #adds these to the list
print lowrain


# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
