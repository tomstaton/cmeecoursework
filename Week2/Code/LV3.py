""" Time-discrete version of the Lotka-Volterra Model simulated using scipy """

import sys
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import numpy

# Define parameters from commandline:
r = float(sys.argv[1]) # Resource growth rate
a = float(sys.argv[2]) # Consumer search rate (determines consumption rate)
z = float(sys.argv[3]) # Consumer mortality rate
e = float(sys.argv[4]) # Consumer production efficiency
K = float(sys.argv[5])

tmax = 10
R = 10.
C = 5.

x = sc.array([[R,C,0]])

## output R and C population sizes for each integer time value:
for t in range(0,tmax): 
	temp = max(0,R) #max(0,*) prevents negative population sizes
	R = max(0,(R*(1+r*(1-(R/K)))-(a*C)))
	C = max(0,(C*(1 - z + (e*a*temp))))
	x = sc.append(x, [[R,C,t]], axis = 0)
	print 'R=', R, 'C=', C

		
f1 = p.figure() #Open empty figure object
p.plot(x[:,2], x[:,0], 'g-', label='Resource density') # Plot
p.plot(x[:,2], x[:,1] , 'b-', label='Consumer density')
p.legend('r')
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
p.show()
f1.savefig('/home/tom/cmeecoursework/Week2/Results/prey_and_predators_LV3.pdf') #Save figure

