"""Aligns sequences for any two inputs"""

__author__ = 'Tom Staton'
__version__ = '0.0.1'

#!/usr/bin/env python
	
import sys
import csv

## Assign 'f' to first input or default
try:
	f = open(sys.argv[1],'rb')
	print 'Inputting ', sys.argv[1]
except IndexError:
	f = open('../../Week1/Data/407228326.fasta','rb')
	print 'Inputting default file 407228326.fasta'

## Assign 'g' to second input or default
try:
	g = open(sys.argv[2],'rb')
	print 'Inputting ', sys.argv[2]
except IndexError:
	g = open('../../Week1/Data/407228412.fasta','rb')
	print 'Inputting default file 407228412.fasta'

## Assign 'h' as the output file
h = open('../../Week2/Data/align-fasta-out.txt', 'wb')

## Assign 'f' and 'g' to Seq1 and Seq2
for line in f:
	seq1=line 
for line in g:
	seq2=line

print "Adding output to 'align-fasta-out.txt'"

# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
	s1 = seq1
	s2 = seq2
else:
	s1 = seq2
	s2 = seq1
	l1, l2 = l2, l1 # swap the two lengths

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
	# startpoint is the point at which we want to start
	matched = "" # contains string for alignement
	score = 0
	for i in range(l2):
		if (i + startpoint) < l1:
			# if its matching the character
			if s1[i + startpoint] == s2[i]:
				matched = matched + "*"
				score = score + 1
			else:
				matched = matched + "-"

	# build some formatted output
	h.write(str("." * startpoint + matched) + '\n')         
	h.write(str("." * startpoint + s2) + '\n')
	h.write(str(s1) + '\n')
	h.write(str(score) + '\n') 
	h.write(str("") + '\n')

	return score

calculate_score(s1, s2, l1, l2, 0)
calculate_score(s1, s2, l1, l2, 1)
calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1):
	z = calculate_score(s1, s2, l1, l2, i)
	if z > my_best_score:
		my_best_align = "." * i + s2
		my_best_score = z

h.write(str(my_best_align) + '\n')
h.write(str(s1) + '\n')
h.write(str("Best score:"))
h.write(str(my_best_score) + '\n')

