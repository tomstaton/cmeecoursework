"""Latin names, common names and body masses of bird species"""

__author__ = 'Tom Staton'
__version__ = '0.0.1'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

#Latin names:
Latin = [x[0] for x in birds]
print Latin

#Common names:
Common = [x[1] for x in birds]
print Common

#Body mass:
Mass = [x[2] for x in birds]
print Mass
	

# (2) Now do the same using conventional loops (you can shoose to do this 
# before 1 !). 

#Latin names:
Latin_names = [] #create empty list
for line in birds: 
	Latin_names.append(tuple(line)) #add tuples to list
	print line[0] #print 1st value
	
#Common names:
Common_names = [] #create empty list
for line in birds: 
	Common_names.append(tuple(line)) #add tuples to list
	print line[1] #print 2nd value

#Body masses names:
Body_mass = [] #create empty list
for line in birds: 
	Body_mass.append(tuple(line)) #add tuples to list
	print line[2] #print 3rd value

