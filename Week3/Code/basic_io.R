#A simple R script to illustrate R input-output.

MyData = read.csv("../Data/trees.csv", header = TRUE) # import with headers

write.csv(MyData, "../Results/MyData.csv") # write as a new file

write.table(MyData[1,], file = "../Results/MyData.csv",append = TRUE) #Append to it

write.csv(MyData, "../Results/MyData.csv", row.names=TRUE) # write row names

write.table(MyData, "../Results/MyData.csv", col.names=FALSE) # ignore column names
