#!/bin/bash
# Author: Tom Staton T.Staton@pgr.reading.ac.uk
# Script: run_get_TreeHeight.sh
# Desc: run tree height script using R
# Arguments: none
# Date: 16 October 2017 

Rscript get_TreeHeight.R trees.csv

#exit
